#
# choose compiler and flags to pass to it
#
CXX = g++
LD = $(CXX)
DEBUG = -ggdb
STD_CONFORM = -std=c++11
WARNINGS = -Wall -Wextra -pedantic

#
# internal setup
#
CXXFLAGS = $(DEBUG) $(STD_CONFORM) $(WARNINGS)
LIBS =

#
# files to compile
#
SOURCES = $(wildcard *.cc)
EXES = $(SOURCES:.cc=.x)

#
# targets that aren't files-to-be-created
#
.PHONY: all clean

#
# tell make about C++ source and resultant executables
#
.SUFFIXES: .cc .x

#
# how to compile & link C++ 
#
.cc.x:
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^ $(LIBS)

#
# main targets (default: all)
#
all:	$(EXES)

clean:
	rm -f $(EXES)
