#include <iostream>
#include <vector>

typedef std::vector<int> vint;

int
main()
{
    vint C(4);

    for (auto& val: C) {
        val += 6;
    }

    for (auto val: C) {
        val += 6;
    }

    for (auto val: C) {
        std::cout << val << "\n";
    }
}
