#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>

typedef float dval;

typedef std::vector<dval> myvec;

static void
dump(std::ostream& o, const myvec& V, size_t n)
{
    o << n << ":";
    for (const auto v : V) {
        o << std::setw(6) << v;
    }
    o << "\n";
}

static void
swap(dval& a, dval& b)
{
    const auto t = a;

    a = b;
    b = t;
}

static size_t
bubsort(myvec& V)
{
    const size_t nv = V.size();
    size_t nswaps = 0;

    for (size_t pass = 0; pass < nv; ++pass) {
        size_t curswaps = 0;

        for (size_t i = 0; i < nv - 1; ++i) {
            if (V.at(i) > V.at(i + 1)) {
                swap(V.at(i), V.at(i + 1));

                dump(std::cout, V, pass);

                ++curswaps;
            }
        }

        if (curswaps == 0) {
            break;
        }

        nswaps += curswaps;
    }

    return nswaps;
}

int
main(int argc, char *argv[])
{
    myvec a;

    for (auto i = 1; i < argc; ++i) {
        a.push_back(std::stof(argv[i]));
    }

    size_t n = bubsort(a);

    std::cout << "Completed with " << n << " swaps\n";

}
