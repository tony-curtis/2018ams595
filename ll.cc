#include <iostream>
#include <list>

typedef std::list<int> lint;

int
main()
{
    const int A[] = {1, 4, 5, 3, 12, 14, 7, 8};
    lint v;

    for (const auto a : A) {
        v.push_front(a);
    }

    for (const auto i : v) {
        std::cout << i << "\n";
    }

    auto f = v.front();

    v.pop_front();

    std::cout << "Front is " << f << "\n";

    for (const auto i : v) {
        std::cout << i << "\n";
    }

}
