#include <iostream>
#include <iomanip>
#include <vector>
#include <cstdlib>

typedef std::vector<unsigned> numvec;

static void
print_line(const numvec& vec, unsigned indent)
{
    std::cout << std::setw(indent) << " ";
    for (auto v : vec) {
        std::cout << std::setw(5) << v;
    }
    std::cout << "\n";
}

static void
next_line(const numvec& oldv, numvec& newv)
{
    const unsigned s = oldv.size();

    newv.push_back(1);
    for (unsigned j = 1; j < s; j++) {
        newv.push_back(oldv.at(j - 1) + oldv.at(j));
    }
    newv.push_back(1);
}

int
main(int argc, char *argv[])
{
    unsigned nrows = 6;

    if (argc > 1) {
        nrows = atoi(argv[1]);
    }

    numvec oldv;

    oldv.push_back(1);

    for (unsigned row = 0; row < nrows; row++) {

        // triangle layout
        const unsigned ind = 10 + ((nrows - row) * 2);

        print_line(oldv, ind);

        numvec newv;

        next_line(oldv, newv);

        // update
        oldv = newv;
    }
}
