#include <iostream>
#include <unordered_map>

typedef std::unordered_map<std::string, unsigned> sumap;

int
main()
{
    const std::string A[] = {
        "george",
        "elaine",
        "newman",
        "delores",
        "george",
        "jerry"
    };
    sumap m;

    for (const auto a : A) {
        if (m[a] > 0) {
            std::cout << "Already seen " << a << "\n";
        }
        ++m[a];
    }

    for (const auto it : m) {
        std::cout << it.first << " -> " << it.second << "\n";
    }
}
